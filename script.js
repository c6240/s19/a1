let trainer = {
	name:`Ash Ketchum`
	age:10
	pokemon: `Pikachu`, `Charizard`
	friends ={ 
	kanto: [`Brock`,`Misty`],
	hoenn: [`May`,`Max`]
}
};

trainer.name = `Ash Ketchum`;
trainer.age = 10
trainer.pokemon = [`Pikachu`, `Charizard`]
trainer.friends = {
	kanto:[`Brock`,`Misty`],
	hoenn:[`May`,`Max`]
}

trainer.talk = () => {
	console.log(`Pikachu I chose you!`)
}


console.log(trainer.name);
console.log(trainer.age);
console.log(trainer.talk());


function Pokemon(name,lvl){
	this.name = name
	this.level = lvl
	this.health = lvl * 2
	this.attack =lvl
	this.tackle = (target) => {
		target.health -= this.attack
	}; 
	this.intro = function (opponent){
		console.log(`Hi Im ${this.name} and this is ${opponent.name}`);
	};
	this.faint = (target) => {
		console.log(`${target.name} has fainted`);
	}

}
let pikachu = new Pokemon(`Pikachu`, 12);
console.log(pikachu);
let geodude = new Pokemon(`Geodude`, 8);
console.log(geodude);
let mewtwo = new Pokemon(`Mewtwo`, 100);
console.log(mewtwo);


